<?php

namespace App;

use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Nota extends Model {
    protected $fillable = [
        'id',
        'title',
        'description',
        'created_at',
        'updated_at',
        'important'
    ];
    
    //protected $dateFormat = 'U';
    // Sobre escribiendo este método definimos el formato en el que se ingresan las fechas en la base de datos, en este caso queremos que sea en formato Unix.
    public function getDateFormat()
    {
      
        return 'U';
    }
    /*  Sobre escribiendo este método definimos el formato que tendrá la fecha cuando se recupere de la base de datos y se envíe al cliente
        en este caso queremos que sea en formato Unix.
    */
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('U', $date)->format('U');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('U', $date)->format('U');
    }
}

<?php

declare(strict_types=1);

namespace App\GraphQL\Queries;

use App\Nota;
use Closure;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Mockery\Matcher\Not;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;
use Rebing\GraphQL\Support\SelectFields;

class NotasQuery extends Query
{
    protected $attributes = [
        'name' => 'NotasQuery',
        'description' => 'Consulta que obtiene todas las notas'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('nota'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'important' => [
                'name' => 'important',
                'type' => Type::boolean()
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        /** @var SelectFields $fields */
        $fields = $getSelectFields();
        $select = $fields->getSelect();
        $with = $fields->getRelations();

        if(isset($args['id'])) {
            $nota = Nota::where('id', $args['id']);
            return $nota->get();
            //return Nota::where('id', $args['id']->get());

        }

        if(isset($args['important'])) {
            $nota = Nota::where('important', $args['important']);
            return $nota->get();
        }

        $notas = Nota::with($with)->select($select)->get();

        return $notas;
    }
}

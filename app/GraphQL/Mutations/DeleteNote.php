<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Nota;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;


class DeleteNote extends Mutation
{
    protected $attributes = [
        'name' => 'deleteNote',
        'description' => 'Delete a note'
    ];

    public function type(): Type
    {
        return GraphQL::type('nota');

    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $nota = Nota::find($args['id']);

        if (!$nota) {
            return null;
        }
        $nota->delete();
        return $nota;
    }
}

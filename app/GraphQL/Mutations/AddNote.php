<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Nota;
use Closure;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;
use Rebing\GraphQL\Support\Facades\GraphQL;

class AddNote extends Mutation
{
    protected $attributes = [
        'name' => 'addNote',
        'description' => 'Insert a new note'
    ];

    public function type(): Type
    {
        return GraphQL::type('nota');
    }

    public function args(): array
    {
        return [
                'title' => [
                    'name' => 'title',
                    'type' => Type::string()
                ],
                'description' => [
                    'name' => 'description',
                    'type' => Type::string()
                ],
                'important' => [
                    'name' => 'important',
                    'type' => Type::boolean()
                ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $nota = new Nota(
            [
                'title'=> $args['title'],
                'description'=>  $args['description'],
                'important' => $args['important']
            ]
            );
        $nota->save();
        return $nota;
       
    }
}

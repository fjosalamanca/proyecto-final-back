<?php

declare(strict_types=1);

namespace App\GraphQL\Mutations;

use App\Nota;
use Closure;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Rebing\GraphQL\Support\SelectFields;

class UpdateNote extends Mutation
{
    protected $attributes = [
        'name' => 'updateNoteDescription',
        'description' => 'A mutation'
    ];

    public function type(): Type
    {
        return GraphQL::type('nota');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int()
            ],
            'description' => [
                'name' => 'description',
                'type' => Type::string()
            ],
            'title' => [
                'name' => 'title',
                'type' => Type::string()
            ],
            'important' => [
                'name' => 'important',
                'type' => Type::boolean()
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        $nota = Nota::find($args['id']);
        if (!$nota) {
            return null;
        }
        $nota->title = $args['title'];
        $nota->description = $args['description'];
        $nota->important = $args['important'];
        $nota->save();
        return $nota;
    }
}

<?php

declare(strict_types=1);

namespace App\GraphQL\Types;

use App\Nota;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class NotaType extends GraphQLType
{
    protected $attributes = [
        'name' => 'NotaType',
        'description' => 'A type',
        'model' =>Nota::class,
    ];

    public function fields(): array
    {
        return [
                'id' => [
                    'type' => Type::nonNull(Type::int()),
                    'description' => 'id de la nota',
                ],
                'title' => [
                    'type' => Type::string(),
                    'description' => 'titulo de la nota',
                ],
                'description' => [
                    'type' => Type::string(),
                    'description' => 'contenido de la nota',
                ],
                'created_at' => [
                    'type' => Type::string(),
                    'description' => 'Fecha de creacion de la nota',
                ],
                'updated_at' => [
                    'type' => Type::string(),
                    'description' => 'Fecha de modificacion de la nota',
                ],
                'important' => [
                    'type' => Type::boolean(),
                    'description' => 'Si es o no importante la nota',
                ]
                
        ];
    }
}
